## Foreword

Foreword
Kanban is a method that shows us how our work works.
It brings us a shared understanding of the work we do, including the rules by which we do the work, how much we can handle at a time, and how well we deliver work to our internal and external customers.
Once we achieve this understanding, we can start to improve. We can become more predictable and work at a more sustainable pace. Communication and collaboration goes up. So does quality. The people doing the work can act more independently because they develop an innate understanding of risk management.
We can also use Kanban to achieve better alignment across our entire enterprise, meaning that sweeping strategic goals can be achieved.
Kanban's focus on managed commitment and a balanced flow of work leads to greater agility. If market conditions change or issues with dependencies arise, Kanban provides the ability to change course quickly. This is why we call it the Alternative Path to Agility.
In 2011, Lean Kanban University set out to establish a standard of quahty for the way Kanban is taught and practiced. Today we have a curriculum of Kanban training at all levels, which includes professional development programs as well as community events and resources. A global network of LKU trainers and coaches assures the quahty and consistency of Kanban and the ongoing evolution of this body of knowledge.
This short book covers core concepts as we understand Kanban. It is based on the contributions of a vibrant global community committed to Kanban and to doing all that it can to improve the world of work.
Janice Linden-Reed President, Lean Kanban, Inc.
