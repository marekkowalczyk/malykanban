## Kanban Roles

Kanban Roles
Kanban is and remains the "start with what you do now" method, where initially no one receives new roles, responsibilities, or job titles. So there are no required roles in Kanban and the method does not create new positions in the organization. However, two roles have emerged from common practice in the field and are now defined in the method itself. It is the purpose of the roles that is important, rather than assigning someone a job title, so it may be helpful to think of the roles as "hats" people wear in carrying out these functions:
The Service Request Manager is responsible for understanding the needs and expectations of customers, and for facilitating selecting and ordering work items at the Replenishment Meeting. Alternative names for the role are Product Manager, Product Owner, and Service Manager.
The Service Delivery Manager is responsible for the flow of work in delivering selected items to customers and for facilitating the Kanban Meeting and Delivery Planning. Alternative names for this role are Flow Manager, Delivery Manager, or even Flow Master.
