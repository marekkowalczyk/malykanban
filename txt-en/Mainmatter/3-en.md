## Kanban Agendas

It might be considered that Kanban, as a "start with what you do now" method, comes with no agenda as to the type or purpose of the change it initiates. In fact, Kanban recognizes three agendas — three compelling calls to action based on organizational need:

FIXME The Sustainability Agenda looks inward to the organization. Its goal is to 

The Sustainability Agenda is about finding a sustainable pace and improving focus.

The Service Orientation Agenda focuses attention on performance and customer satisfaction.

The Survivability Agenda is concerned with staying competitive and adaptive.

TODO Figure 2. Kanban's agendas

FIXME services that are not overburdened with work, but that balance demand with capability, thereby improving the performance of services with regard to customer satisfaction, staff engagement and collaboration, and cost. It is a natural starting point for change because, in situations where demand outstrips capability, making intangible work visible and reducing overburdening in the servicing of the work is likely to have an immediate positive impact on the amount of work completed, the time needed to complete work items, and staff morale.

The Service Orientation Agenda looks outward from the organization's purpose to its customers. It should be the clearest and most explicit agenda for all organizations. The goal is to provide services to customers that are fit for purpose—that meet and exceed customers'needs and expectations. This should be viewed as transcending sub-goals such as profitability or returning value to shareholders, which ultimately are merely means to that end. When everyone in the organization—every department and every service—focuses on providing service to their customers, the organization itself will achieve outstanding results. Kanban is about delivering services and improving them, and the Service Orientation Agenda is a key to its success.

The Survivability Agenda looks forward to the future. It seeks to ensure that an organization survives and thrives in times of significant change. The pace of change and the emergence of disruptors in all major markets means that no organization can assume current processes and technology will suffice for even a few years into the future. Kanban's evolutionary approach to change—with its focus on safe-to-fail, continuous improvement; encouraging diversity in processes and technology; and respect and engagement for all stakeholders involved—is an appropriate response to this constant challenge.
