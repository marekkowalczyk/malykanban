## The General Practices of Kanban

The General Practices of Kanban define essential activities for those managing kanban systems (Figure 6). There are six of them.

Visualize.
Limit work in progress.
Manage flow.
Make policies explicit.
Implement feedback loops.
Improve collaboratively, evolve experimentally.

These practices all involve: 

seeing the work and the policies that determine how it is processed; then
improving the process in an evolutionary fashion —keeping and amplifying useful change and learning from and reversing or dampening ineffective change.	

TODO Figure 6 Kanban's practices

Let's look at each of the general practices in more detail.

Visualize

A kanban board such as in the diagram in Figure 4 (on page 13) is one, though not the only, way to visualize work and the process it goes through. For it to be a kanban system rather than simply a flow system, the commitment and delivery points must be defined, and WiP Limits must be displayed to limit the work in progress at each stage between these points. The act of making work and policies visible—whether on a wall board, in electronic displays, or other means—is the result of an important journey of collaboration to understand the current system and find potential areas for improvement.

Policies, too, are important to visualize; for example, by placing summaries between columns of what must be done before items move from one column to the next.

Board design varies greatly among kanban systems, depending on how they are used (see, for example, the Kanbanraum in Figure 6). The method does not constrain how to design them. Software tools developed to support Kanban may introduce practical constraints— for example, the common pattern of a two-dimensional grid with panels displaying information about each work item. The columns represent steps in a process, and some of the columns have horizontal partitions (called swimlanes, if they cross two or more columns) to distinguish states of items within the steps. However it is interesting to note that teams designing physical boards, without such constraints, often find other creative ways to display information of importance to them, including connections to boards belonging to other services.

Figure 6 The "Kanbanraum" at Visotech uses many different kinds of visualizations to see the work, the type of work, and the effort. 8

Design of the card or panel that describes the work item is another essential aspect of visualization. It is also vital to highlight visually when items are blocked by dependencies on other services or for other reasons.

Limit Work in Progress

Introducing and respecting Hmits on WiP changes a "push" system into a "pull" system, in which new items are not started until work is completed (or on rarer occasions, aborted). Having too much partially complete work is wasteful and expensive and, crucially, it lengthens lead times, preventing the organization from being responsive to their customers and to changing circumstances and opportunities.

Observing, limiting, and then optimizing the amount of work in progress is essential to success with Kanban (Figure 7), as it results in improved lead time for services, improved quality, and a higher rate of deliveries. 9

By contrast, ineffective management behavior focuses on maximizing the usage of people and resources by trying to ensure that everyone is "busy"with a ready supply of work so that no idle time occurs.10 As a result, people may feel overwhelmed with the amount they have to do and accept only tasks they have been explicitly instructed to carry out; they may lose sight of the service they provide and how it contributes to the overall goals of the organization and its customers.

LET'S DO SOMETHING ABOUT IT.'

TODO Figure 7 Limiting WiP provokes discussion and improvement, (from the cover of [Anderson, 2010])

Manage Flow

The flow of work in a kanban system should maximize the delivery of value, minimize lead times, and be as smooth (i.e. predictable) as possible. These are sometimes conflicting goals and, since the deliverables are usually complex, empirical control through transparency, inspection, and adaption is required. Bottlenecks, where flow is constrained by one particular sub-process, and blockers, where there are dependencies on other services, are important to take note of and manage.

A key to understanding and maximizing the flow of value is the cost of delay of work items. This is the amount of an item's value that is lost by delaying its implementation by a specified period of time. In general, the cost of delay is a function of time (which may or may not be linear), and the rate at which value changes (the cost of delay per time period, or urgency) may or may not be constant over time. Kanban uses four archetypes to characterize how the value of items changes with delay: expedite, fixed date, standard, and intangible (see Figure 8).

These archetypes may be used to assist in ordering work items, or they may define different classes of service, where different policies are applied to different types of work. 11

TODO Figure 8 Graphs depicting cost of delay

The relationship with the consumers of the service, the customers, is a key aspect of managing flow. Lead Time, especially Customer Lead Time, is a key metric for customers, though many other aspects are important, including: Delivery Rate, defect rate (and other quahty measures), and predictability of supply. Different service levels such as the following may be defined for kanban systems to guide this.

Service Level Expectation what the customer expects
Service Level Capability what the system can deliver
Service Level Agreement what is agreed with the customer
Service Fitness Threshold the service level below which the service delivery is unacceptable to the customer

Make Policies Explicit

Explicit policies are a way of articulating and defining a process that goes beyond the workflow definition. A process expressed as workflow aw^policies creates constraints on action, is empowering within the constraints, and results in emergent characteristics that can be tuned by experiment. The process policies need to be sparse, simple, well-defined, visible, always applied, and readily changeable by those providing the service. Note that "always applied" and "readily changeable" go together. Setting WiP limits, and then never challenging, changing, or breaking the limits to see whether different limits in differing circumstances improve outcomes, would be a poor application of this practice.

The behavior of complex systems, though they may be guided by simple policies, is not possible to predict. Policies that may seem intuitively obvious (for example, "the sooner you start, the sooner you'll finish") often produce counterintuitive results. For this reason, it is a core practice to make explicit the policies that apply to services and for there to be a visible and straightforward mechanism to question and change policies when they are considered counterproductive or are found not to be applied.

TODO Figure 9 Policies for different stages of work (above each column)

WiP Limits are one type of policy; others include capacity allocation and balancing, the "Definition of Done," or other policies for work items exiting the stages of a process (see Figure 9). Replenishment policies for selecting new work when capacity is available and using classes of service are additional policy examples.

Implement Feedback Loops

Feedback loops are an essential part of any controlled process and are especially important for evolutionary change. Improving feedback in all areas of the process is important, but it is particularly so in the following:

strategy alignment
operational coordination
risk management
service improvement
replenishment
flow
customer deliveries

Kanban defines seven specific feedback opportunities, or cadences. Cadences are the cyclical meetings and reviews that drive evolutionary change and effective service delivery. "Cadence" may also refer to the time period between reviews—one workday or one month, for example. Choosing the right cadence is context dependent and it is crucial to good outcomes. Too-frequent reviews may compel changing things before seeing the effect of previous changes, but if they are not frequent enough, poor performance may persist longer than necessary.

TODO Figure 10. A set of cadences showing feedback loops

A scheme of seven cadences, depicted in Figure 10, shows suggested frequencies for the reviews in a typical enterprise or multiple-service context.

Strategy Review This is for selection of the services to be provided and to define for this set of services the concept of "fit for purpose"; also for sensing how the external environment is changing in order to provide direction to the services.

Operations Review This is to understand the balance between and across services, deploying resources to maximize the delivery of value aligned with customers' expectations.

Risk Review This review is to understand and respond to the risks to effective delivery of services; for example, through blocker clustering.

Service Delivery Review This is to examine and improve the effectiveness of a service (this and subsequent cadences apply to a single service).

Replenishment Meeting This meeting is for moving items over the commitment point (and into the system) and to oversee the preparation of options for future selection.

The Kanban Meeting This is the (usually) daily coordination, self-organization, and planning review for those collaborating to deliver the service. It often uses a "stand-up"format to encourage a short, energetic meeting with the focus on completing work items and unblocking issues.

Delivery Planning Meeting This is to monitor and plan deliveries to customers.

Implementing the seven cadences does not imply adding seven new meetings to an organization's overhead, although the Replenishment and Kanban Meetings are considered a baseline in nearly all Kanban implementations. Initially, the agenda of each cadence should be part of existing meetings and adapted in context to fulfill their goals. On a smaller scale, a single meeting may cover more than one cadence.

The feedback loops in the cadence network diagram (Figure 10) show example information flow and requests for change between the reviews. These facilitate decision making at each level.

Improve Collaboratively, Evolve Experimentally

Kanban is fundamentally an improvement method. Often, transformation programs are started with the aim to change processes to a new, predefined approach. In contrast, Kanban starts from the organization as it is now and uses the Lean flow paradigm12 (seeing work as a flow of value) to pursue continuous and incremental improvement. There is no endpoint of such change processes since perfection in an ever-changing fitness landscape is unattainable. Kanban harnesses an evolutionary process to allow beneficial change to occur within an organization, protecting it from another natural evolutionary process—extinction! Organizations cannot opt out of evolution: It either works for them or happens to them. But they can choose to encourage the change to occur from within, rather than finding it is unable to respond to existential threats from without. Kanban facilitates this.

The evolutionary process involves differentiation (copying with deliberate differences or mutations); selecting for fitness; and keeping and amplifying useful change while dampening or reversing ineffective change. 13

It can be useful to employ models and the scientific method to validate or invalidate applying the models in context. Sometimes using empirical and pragmatic approaches is an appropriate way to find the greatest fitness for purpose within the current environment.
