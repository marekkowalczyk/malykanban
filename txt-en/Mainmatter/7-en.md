## Introducing Kanban to Organizations

Introducing Kanban to Organizations
It is simple to start using Kanban: Recognize that your work involves a flow of value from the request for an item to its delivery to your customer; visualize the work and the process for delivering the work; then continually improve the process by applying the values, the principles, and the practices.14
All through this process you will be applying Kanban, even while the characteristics of your systems are barely different from your starting point. Clearly, this means that there are organizations applying Kanban that do not yet even have a kanban system (a system that limits work in progress with visual signals), or whose kanban systems have not yet matured, for example, to an effective balancing of demand with capability through feedback loops, or to optimal value delivery through classes of service.
Such systems may be referred to as protokanban systems because they are systems being transformed by Kanban, though not yet compliant with its general practices.15 Protokanban systems can bring great benefit to organizations—for example, in making intangible work visible—but they should not be viewed as endpoints in process transformations.
For these reasons, the Kanban Method defines an approach for introducing Kanban (STATIK) and a test for assessing your progress with Kanban (the Litmus Test).
27
Systems Thinking Approach to Introducing Kanban (STATIK)
Systems Thinking16 is a way of understanding how a system behaves as a whole rather than through analysis of isolated component parts. It is a key influence in defining the steps needed to introduce Kanban in an organization. The steps in this process are not necessarily sequential, but iterative—using learning from one step to inform and influence the others in a collaborative environment. The steps are as follows.
Step 0 Identify services.
For each service . . .
Step 1 Understand what makes the service fit for purpose for the customer.
Step 2 Understand sources of dissatisfaction with the current system.
Step 3	Analyze demand.
Step 4	Analyze capability.
Step 5	Model workflow.
Step 6	Discover classes of service.
Step 7	Design the kanban system.
Step 8 Socialize the system and board design and negotiate implementation.
STATIK is applicable to just one service. When more than one service has been set up, Kanban practices and cadences are applied to balance demand and flow across the multiple services and to continually improve. The emphasis on Systems Thinking here is
28	Introducing Kanban to Organizations
important. If services are improved in isolation, this results in "sub-optimization." The system as a whole, with its goal of improving the flow of value to customers, must be considered. Sometimes this means that the first services to address with STATIK might be those that operate at a higher level and are delivering directly to customers, rather than internal services delivering within the organization.
In practice, the order of the steps in STATIK may vary, and it is normal to revisit steps in pursuit of further improvement.
The Kanban Litmus Test
The Kanban Litmus Test is designed to help organizations assess their progress with Kanban and suggest areas that may yield effective improvements. It consists of a series of four questions; the first questions are prerequisites for those that follow.
Has management behavior changed to enable Kanban?
Has the customer interface changed in line with Kanban?
Has the customer contract changed informed by Kanban?
Has your service delivery business model changed to exploit Kanban?
1. Management Behavior
An organization adopting Kanban needs managers who respect kanban system policies, embrace customer focus as a value, and manage work in line with the service delivery principles.
Ask these supplementary questions . . .
• Is management behavior consistent with Kanban's deferred commitment, pull system approach?
Introducing Kanban to Organizations	29
Are WiP limits respected by management at the system level, not just at a personal level (such as per-person WiP limits to reduce multitasking)?
Is customer focus always an understood reason for change?
2.	Customer Interface
The services in the organization need true kanban systems with deferred commitment and a Replenishment Meeting to schedule, sequence, and select work. This provides a customer interface focused on maximizing the flow of value within the constraints of current capability.
Ask these supplementary questions . . .
Is the approach to scheduling and selecting customer requests based on a pull system with limited work in progress?
Are the commitment and delivery points clearly defined and are records of Lead Times and Delivery Rates available?
Is there a regular Replenishment Meeting?
3.	Customer Contract
The customer contract, whether a formal service level agreement or an understood service level expectation, should be based on measured performance of the service, such as Lead Times and Delivery Rates.
Ask these supplementary questions . . .
Are commitments made to the customer based on the agreed or understood service levels (explicit service level agreements or service level expectations)}
Are these levels based on probabilistic forecasting using the kanban system's observed Lead Times and Delivery Rates?
30	Introducing Kanban to Organizations
4. Service Business Model
In services with established kanban systems, improved value and risk management is possible, for example, through classes of service, capacity allocation, demand shaping, and differential pricing.
Ask these supplementary questions . . .
Does the service delivery business model use classes of service appropriately, based on an understanding of business risks (for example, the cost of delay) to facilitate selection decisions and inspire queuing discipline policies for work items? Are you understanding customer expectations and how they cluster into similar groups? Are you probing for possible new classes of service to improve the flow of value to the customer?
Is there capacity in the system to hedge risks from different sources of demand and different types of work? For example, can resources be diverted to priority tasks during high-demand periods?
Are interdependent services aggregated and coordinated to increase system liquidity and enable system leveling in light of risks and variability?
Introducing Kanban to Organizations	31