## Describing Flow Systems

Kanban is used to define, manage, and improve systems that deliver services of value to customers. As Kanban is applied to knowledge work, where the deliveries consist of information in various forms rather than physical items, the processes can be defined as a series of knowledge-discovery steps and their associated policies, made visible in a kanban board such as the one in Figure 4.

The board depicts a flow system in which work items flow through various stages of a process, ordered from left to right.

Several conditions must exist for this flow system to be a kanban system. First, there must be signals (usually visual signals) to limit work in progress (WiP). In this case, the signals derive from the combination of the cards, the displayed Work in Progress Limits (in the rectangles at the head of the columns), and the column that represents the activity. In addition, kanban systems must have identified commitment and delivery points.

TODO Figure 4. An example of a kanban board

The commitment is an explicit or tacit agreement between customer and service that:

the customer wants an item and will take delivery of it, and
the service will produce it and deliver it to the customer.

Before the commitment point there may be a set of outstanding requests (or a pool of ideas), which may or may not be selected, and a process whose purpose is selecting items from these options. Kanban applied to processes prior to the commitment point is sometimes referred to as Discovery Kanban.3 The delivery point is where items are considered complete.

The time that an item is in process between the commitment and delivery points is referred to as the item's Lead Time (or System Lead Time). Customer Lead Time may be different—it is the time a customer waits for the item (typically from request to receipt). The fact that a distinction is made between the creation, or arrival, of a request and the commitment to fulfill the request is important; it is referred to as deferred commitment. Anomalies in the definition of system lead time and customer lead time occur for two reasons: the customer has not agreed to adopt a pull system and still pushes work for delivery regardless of capacity or capabiHty to process it; the service dehvery is internal to a wider network of services and not directly coupled to the originating customer request, hence the internal requesting "customer" has already committed to the work and the receiving service has no option other than to make their best effort to process it in a timely manner.

The collection of items that are within the system under consideration at any point in time, as well as the count of the number of these items, is known as the Work in Progress or WiP.

The rate at which items are deHvered is known as the Delivery Rate. This is calculated from the reciprocal of the time between the latest delivery and the previous one or, for an average Delivery Rate over a given period, by dividing the number of deliveries by the length of the time period.

Little's Law

In a flow system that is not trending4 (and in which all items that are selected are delivered) there is a simple relationship between the averages of these metrics over a specific period. It is known as Little's Law:

TODO Formulas

WiP
Delivery Rate —
Lead Time where the overline denotes arithmetic mean.

We may wish to use Little's Law to examine the flow metrics of other parts of a kanban system—not just between the commitment and delivery points—in which case, rather than Lead Time we use Time in Process orTiP6 for the period an item is in the process under consideration. More specific terms such as Time in Development, Time in Test, Time in System (synonymous with System Lead Time) or Time in Queue may also be used.

The term Throughput is used instead of Delivery Rate if the end of the process under consideration is not the delivery point. 7

Here is an alternative formulation of Little's Law using these terms:

TODO Formulas 2

WiP
Throughput =

Little's Law many be demonstrated graphically on a Cumulative Flow Diagram, as shown in Figure 5, which plots the cumulative number of items arriving and departing from a system.

The Approximate Average Lead Time (Approx Av. LT) and the Approximate Average WiP (Approx Av. WiP) are marked on the diagram. The slope of the hypotenuse of the marked triangle is the Average Delivery Rate over this period and, in line with Little's Law, it can be seen to be:

TODO Formula 3 ApproxAv. WiP ApproxAv. LT

The actual averages for Lead Time and WiP have to be calculated from individual items, but in non-trending systems they will approximate to these values.

Little's Law provides an important insight into kanban systems: in order to optimize the Lead Time for work items, we must Hmit the Work in Progress. This is one of the General Practices of Kanban.

Cumulative Flow Diagram (CFD)

TODO Figure 5. A Cumulative Flow Diagram
