## Forecasting and Metrics

33
Forecasting and Metrics
Forecasting accurately when services will be delivered to customers has long been a difficult management problem. Traditionally, projects have used "effort-plus-risk estimating" to forecast completion dates. Kanban systems enable an alternative (some would say more reliable) method—probabilistic forecasting.
Traditional effort-plus-risk approaches break down a large piece of work (like a project) into very small items and then sum the effort estimates for these items. Then either an acceptable date or the team size is agreed upon, which leaves the other variable to be determined by ensuring that the product's required lead time or the team size is greater than the estimated effort by a sufficiently large factor to account for risks and profit. Often this involves a "risk factor" of between 2 and 10. This method has often proven spectacularly unsuccessful on all sizes of projects, but particularly on large and critical ones. Surprisingly, it still is the dominant method of forecasting.
Kanban systems, once established, provide the opportunity to base forecasting on the observed flow of value (encapsulated in much smaller work items than typical projects) delivered through established teams. Probabilistic forecasting works by using a simple model of the existing teams (or similarly structured new ones), where some data has already been gathered on item size variability, lead times, and delivery rates. If there is no data available from similar teams, range estimates can be used until actual data starts flowing. Using a Monte Carlo method, which runs scenarios multiple times, the percentage likelihood of a range of completion dates can be generated. Providing this to planners encourages a better approach to balancing
35
Simulated Burn Downs (first 50 of 1000)
/2016	6/3/2016	6/10/2016
it Completed Dete |on or before)
uncerlalntyin	Simulated Forecast Date Frequency
4/1/2016	4/8/2015	4/15/2016	4/22/2016	4/20/2016	5/6|2'
Date
5/13/2016	5/2' /2016	5/2 /2016	6/3/2016	6/10/2016
85%
Figure 11 Probabilistic Forecast showing the uncertainty in number of "stories" to complete and the Delivery Rate. Completion Dates with 50%, 85%, and 95% probabilities are marked.
cost and risk with schedules and commitments. Figure 11 shows the output from a Monte Carlo model run, showing a selection of the many randomized simulations carried out, and the resulting Forecast Date distribution, which is the basis of the probabilistic forecast.
Designing appropriate service level agreements with customers is also enabled by collecting actual data from kanban systems and applying statistical analysis and probabilistic forecasting.
Flow systems can provide a wide range of flow metrics that are important to the managers of these systems, particularly for producing reliable forecasts.17 The minimum starting point is to collect data on Lead Time, DeHvery Rate, WiP, and cost (usually primarily the effort in person-days consumed by the service).
ProbabiHstic forecasting works best when actual historical data on the performance of services is available (second best is a well considered range estimate). Significant analysis of many types of systems is
36
Forecasting and Metrics
now becoming available, which enables services to predict the shape of lead time or delivery rate distributions.18
The metrics captured in the graphs in Figures 12,13 and 14 were generated simply from the dates when items entered the states of "Committed," "Acceptance," and "Delivered." Some cost data, in terms of either financial costs or person-days, also should be captured.

3.00 2.50 2.00 1.50 1.00 0.50
0.00
1/14/

Scatterplot: Lead Time (weeks)






•
•
< •
i




1
1 <
>
•
• •




•1
• 1
• m
• • •
•
•

• •
•
• • •



• 1
>• •
•
• • •
•
•
• •• •
• • • •
• • • •
•
•• • • •• •




• • •
•
•• i ••• •
• •
•
• •
•
• •





• • • •
•




2015
2/4/2015 2/25/2015 3/18/2015 4/8/2015
4/29/2015

160 140 120 100 80 60 40

Cumulative Flow Diagram (CFD)




































































































































1/14
'2015
2/4/2015 2/25/2015 3/18/2015 4/8/2015 ■ Committed Acceptance ■ Delivered
4/29/2015

Figure 12 Complementary charts of the same flow data Top: Scatterplot of Lead Times for items on their delivery dates
Bottom: Cumulative Flow Diagram showing the cumulative number of items Committed, in Acceptance, and Delivered by date
Forecasting; and Metrics
37
2.50 2.00 1.50
Run Chart: Lead Time (weeks; 7 day average)
1.00 0.50 0.00
	1	1	1	1	1
1/14/2015
2/4/2015
2/25/2015 3/18/2015 4/8/2015 4/29/2015
Run Chart: Delivery Rate (items/week; 7 day average)


1/14/2015	2/4/2015	2/25/2015	3/18/2015	4/8/2015	4/29/2015
» WiP (7 day average) ■ WiP (Daily average)
20 15 10
5
Run Chart: WiP (items)
*. Njr*"^	• "' -..'Sfc*

1/14/2015
2/4/2015
2/25/2015
3/18/2015
4/8/2015
4/29/2015
Control Chart: Age of WiP

■ Average Age of WiP (weeks; 7 day average) Average LT/2 (to date)
- Oldest (weeks)
■ Control: SLA Maximum LT

1/14/2015
2/4/2015
2/25/2015	3/18/2015
4/8/2015
4/29/2015
Figure 13 Run or control charts of LT, DR, WiR and Age of WiP
There are several important types of graphs for displaying flow systems data, including:
• Scatterplots of Lead Times (see Figure 12)
38
Forecasting; and Metrics
Cumulative Flow Diagrams (CFDs), which show the cumulative number of arrivals and departures in a process or in parts of a process (see Figure 12)
Run Charts of average Lead Times, Delivery Rates, WiP, and Age of WiP (see Figure 13)
Control Charts of Lead Times or Age of WiP may also be used. Control charts may be run charts or scatterplots with the addition of control ranges, which may be used to trigger action that keeps items in the desired range. Control charts are more common in manufacturing than in Kanban because of the higher natural and expected variation in knowledge work (see Figure 13).
Distribution Histograms of Lead Times and Delivery Rates (see example in Figure 14)
The run charts in Figure 13 show the variation in the 7-day rolling averages for these metrics over the same period. The Age of WiP chart additionally shows the age of the "oldest" item in progress. It
LEAD TIME HISTOGRAM (DAYS) 23 23

7 8 9 10 11 12 13 14 15 16 17 18 19 20 Value (calendar days from start to complete)
Figure 14 Lead Time distribution histogram
Forecasting: and Metrics
39
is a control chart due to the addition of control lines, which trigger analysis or intervention.
Distribution data is needed for effective probabilistic forecasting. Figure 14 shows an example of a Lead Time distribution histogram. Relying on a single value (like an average) for forecasting or making decisions is problematic because it hides patterns of different types of data and context. Some ranges of values will occur more often than others, and often in a Kanban process there are multiple peaks (very common values) and troughs (rarer values). The peaks commonly represent different types of work, different priority of work, or class of service promises. To improve predictability of a system dehvery to customer, it is important to consider this distribution of values so that the right range is chosen for the work type or class of service you are analyzing.
40	Forecasting and Metrics