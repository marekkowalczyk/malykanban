## The Foundational Principles of Kanban

There are six foundational principles of Kanban, which divided into two groups: the change management principles and the service delivery principles (Figure 3). 

FIXME may

Change Management Principles

Your organization is a network of individuals, psychologically and sociologically wired to resist change. Kanban acknowledges these human aspects with three change management principles:

TODO Figure 3 Kanban's principles

FIXME understanding current processes, as they are actually practiced and
respecting existing roles, responsibilities, and job titles.

Agree to pursue improvement through evolutionary change.
Encourage acts of leadership at every level—from the individual contributor to senior management.

There are two key reasons that "starting from here" is a good idea. The first is that minimizing resistance to change by respecting current practice and practitioners is crucial to engaging everyone in meeting the challenges of the future. The second is that the current processes, along with their obvious deficiencies, contain wisdom and resilience that even those working with them may not fully appreciate. Since change is essential, we should not impose solutions from different contexts, but instead agree to pursue evolutionary improvement across all levels of the organization. Starting from current practice establishes the baseline of performance and effectiveness from which future changes can be assessed.

Service Delivery Principles

Any sizable organization is an ecosystem of interdependent services. Kanban acknowledges this with three service delivery principles, applicable not just to one service but to the whole network:

Understand and focus on your customers' needs and expectations.
Manage the work; let people self-organize around it.
Evolve policies to improve customer and business outcomes.

These principles align closely with the service orientation agenda and the value of customer focus. When the work itself and the flow of value to customers that it represents are not clearly visible, organizations often focus instead on what is visible, the people working on the service. Are they always busy? Are they skilled enough? Could they work harder? The customer and the work items that represent value to the customer receive less attention. What these principles stress is that the focus must move back to the consumers of the service and value they receive from it.
