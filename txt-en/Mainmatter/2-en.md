## Kanban Values

The Kanban Method is values led. It is motivated by the belief that respecting all of the individuals who contribute to a collaborative enterprise is necessary, not only for the success of the venture, but for it to be worthwhile at all.

Kanban's values may be summed up in that single word, "respect." However, it is useful to expand this into a set of nine values2 (including respect) that encapsulates why the principles and practices of Kanban exist (Figure 1).

TODO Figure 1. Kanban's Values

Transparency The belief that sharing information openly improves the flow of business value. Using clear and straightforward vocabulary is part of this value.

Balance The understanding that different aspects, viewpoints, and capabilities all must be balanced for effectiveness. Some aspects (such as demand and capability) will cause breakdown if they are out of balance for an extended period.

Collaboration Working together. The Kanban Method was formulated to improve the way people work together, so collaboration is at its heart.

Customer Focus Knowing the goal for the system. Every kanban system flows to a point of realizing value—when customers receive a required item or service. Customers in this context are external to the service, but may be internal or external to the organization as a whole. The customers and the value they receive is the natural point of focus in Kanban.

Flow The realization that work is a flow of value, whether continuous or episodic. Seeing flow is an essential starting point in using Kanban.

Leadership The ability to inspire others to action through example, words, and reflection. Most organizations have some degree of hierarchical structure, but in Kanban leadership is needed at all levels to achieve value delivery and improvement.

Understanding Primarily self-knowledge (both of the individual and of the organization) in order to move forward. Kanban is an improvement method, and knowing the starting point is foundational.

Agreement The commitment to move together toward goals, respecting—and where possible, accommodating—differences of opinion or approach. This is not management by consensus, but a dynamic co-commitment to improvement.

Respect Valuing, understanding, and showing consideration for people. Appropriately at the foot of this list, it is the foundation on which the other values rest.

These values embody the motivations of Kanban in seeking to improve services delivered by collaborating teams. The method cannot be applied faithfully without embracing them.
