Glossary
Many of the definitions in this Glossary draw significantly, in some cases word for word, from the Kanban Glossary published in Kanban from the Inside (Burrows, 2014) and on the Lean Kanban University web site (LKU, 2015). They are used here with permission.
