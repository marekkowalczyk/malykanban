References
Anderson (2005)
David J. Anderson and Dragos Dumitriu. "From Worst to Best in 9 Months: Implementing a Drum-Buffer-Rope Solution at Microsoft's IT Department." TOC ICO World Conference, November 2005, USA: Microsoft Corporation.
Anderson (2010)
David J. Anderson. Kanban: Successful Evolutionary Change for Your Technology Business. Sequim, WA: Blue Hole Press.
Anderson (2015a)
David J. Anderson. "Introducing Enterprise Services Planning."Lean Kanban Services. http://services. leankanban.com/introducing-enterprise-services-planning (accessed March 18,2016).
Anderson (2015b)
David J. Anderson. "Kanban Enterprise Services Planning: Scaling the Benefits of Kanban." London Limited WLP Society, October 2015. http://www.slideshare.net/agilernanager/ enterprise-services-planning-scaling-the-benetits-of-kanban-54207714 (accessed November 2,2015).
Beinhocker (2007)
Eric D. Beinhocker. The Origin of Wealth: Evolution, Complexity, and the Radical Remaking of Economics. London: Random House Business Books.
67
Benson (2011)
Jim Benson and Tonianne DeMaria Barry. Personal Kanban: Mapping Work, Navigating Life. Seattle, WA: Modus Cooperandi.
Benson (2014)
Jim Benson. Why Limit WIP: We Are Drowning in Work. Seattle, WA: Modus Cooperandi.
Burrows (2014)
Mike Burrows. Kanban from the Inside: Understand the Kanban Method, connect it to what you already know, introduce it with impact. Sequim, WA: Blue Hole Press.
Carmichael (2013)
Andy Carmichael. "Shortest Possible Guide to Adopting Kanban." Improving Projects. http://xprocess.blogspot. co.uk/2013/ 05/how-to-adopt-kanban.html (accessed December 11,2015).
Carmichael (2016)
Andy Carmichael. "Understanding Cost of Delay and Its Use in Kanban.'1'Improvingprojects. http://xprocess. blogspot.co.uk/2016/04/understanding-cost-of-delay-and-its-use.html (accessed April 15,2016).
Deming (2000)
W. Edwards Deming. The New Economics: For Industry, Government, Education, 2nd ed. Cambridge, MA: MIT Press.
Deming (2012)
W. Edwards Deming. The Essential Deming: Leadership Principles from the Father of Total Quality Management, eds. Joyce Orsini and Diana Deming Cahill. New York: McGraw-Hill Professional Publishing.
References
Drucker (1959)
Peter R Drucker. The Landmarks ofTomorrow. New York: Harper & Row.
Dzhambazova (2015)
Irina Dzhambazova. "Kanban Case Study Series." Lean Kanban University, http://leankanban.com/case-studies/ (accessed March 1,2016).
Garcia (2006)
Suzanne Garcia and Richard Turner. CMMI Survival Guide: Just Enough Process Improvement. Upper Saddle River, NJ: Addison-Wesley.
Goldratt (1989)
Eliyahu M. Goldratt and Jeff Cox. The Goal: A Process of Ongoing Improvement. New York: North River Press.
Hammarberg (2014)
Marcus Hammarberg and Joakim Sunden. Kanban in Action. Shelter Island, NY: Manning Publications.
Hopp (2005)
Wallace J. Hopp and Mark L. Spearman. Factory Physics, 3rd ed. Long Grove, IL: Waveland Press.
Kniberg (2010)
Henrik Kniberg and Mattias Skarin. Kanban and Scrum -Making the Most of Both. United States: C4Media Inc. for InfoCX,
Ladas (2009)
Corey Ladas. Scrumban and Other Essays on Kanban Systems for Lean Software Development. Seattle, WA: Modus Cooperandi.
References	69
LKU (2015)
"Glossary of Terms. "Lean Kanban University. http://edu. leankanban.com/kanban-glossary-terms (accessed January 7,2016).
Leopold (2015)
Klaus Leopold and Siegfried Kaltenecker. Kanban Change Leadership: Creating a Culture of Continuous Improvement. Hoboken, NJ: John Wiley & Sons, Inc.
Liker (2004)
Jeffrey K. Liker. The Toyota Way: Fourteen Management Principles from the World's Greatest Manufacturer. New York: McGraw-Hill.
Little (1961)
John D. C. Little. "A Proof for the Queuing Formula: L = AW." Operations Research, 9(3): 383-87.
Little (2011)
John D. C. Little. "Little's Law as Viewed on Its 50th Anniversary." Operations Research, 59(3): 536-49.
Levitin (2015)
Daniel J. Levitin. The Organized Mind: Thinking Straight in the Age of Information Overload. London: Penguin Random House.
Maccherone (2012)
Larry Maccherone. "Introducing the Time In State InSITe Chart.'1'Maccherone.com. LSSC. http://maccherone. com/publications/LSSC2012-IntroducingtheTimeIn StateInSITeChart.pdf (accessed May 27,2015).
70	References
Maccherone (2014)
Larry Maccherone. "The Impact of Lean and Agile Quantified: 2014." Lean Kanban UK2014, London: InfoQ^ http://www.infoq.com/presentations/agile-quantify (accessed February 11,2016).
Maccherone (2015)
Larry Maccherone. "The Impact of Agile, Quantified." CA Technologies, https://www.rallydev.com/resource/ impact-agile-quantified-sdpi-whitepaper (accessed February 10,2016).
Magennis (2011)
Troy Magennis. Forecasting and Simulating Software Development Projects. Focused Objective, http:// focusedobjective.com/training/books-and-pubhcations/ (accessed December 11,2015).
Magennis (2013)
Troy Magennis. "Cycle Time Analytics and Forecasting." Lean Kanban Central Europe 2013, Slide Share, http:// www.sHdeshare.net/Focused Objective/lkce-cycle-time-analytics-and-forecasting-troy-magennis (accessed March 1,2016).
Magennis (2016)
Troy Magennis. "Software Downloads." Focused Objective. http://focusedobjective.com/software/ (accessed March 20,2016).
Meadows (2009)
Donella H. Meadows and Diana Wright. Thinking in Systems: A Primer. London: Taylor & Francis.
References	71
Reddy(2016)
Ajay Reddy. The ScrumBan [R]Evolution: Getting the Most out of Agile, Scrum, and Lean Kanban. Upper Saddle River, NJ: Addison-Wesley.
Reinertsen (2009)
Donald G. Reinertsen. The Principles of Product Development Flow. Redondo Beach, CA: Celeritas Publishing.
Schwaber (2013)
Ken Schwaber and Jeff Sutherland. "The Scrum Guide." Scrum Guides, http://www.scrurnguides.org/scrurn-guide. html (accessed January 1,2016).
Skarin (2015)
Mattias Skarin. Real-World Kanban: Do Less, Accomplish More with Lean Thinking. Frisco, TX: Pragmatic Bookshelf.
Shimokawa (2009)
Koichi Shimokawa and Takahiro Fujimoto, eds. The Birth of Lean: Conversations with Taiichi Ohno, Eiji Toyoda, and Other Figures Who Shaped Toyota Management: 1.0. Cambridge, MA: The Lean Enterprise Institute, Inc.
Shook (2014)
John Shook and Chet Marchwinski, eds. Lean Lexicon: A Graphical Glossary for Lean Thinkers, 5th ed. Cambridge, MA: The Lean Enterprise Institute, Inc.
Steyaert (2014)
Patrick Steyaert. "Discovery Kanban." Okaloa. http:// www.discovery-kanban.com/ (accessed December 11,
2015).
72	References
Taleb (2013)
Nassini Nicholas Taleb. Antifragile: Things That Gain from Disorder. London: Penguin Books.
Vacant! (2015)
Daniel S. Vacant!. Actionable Agile Metrics for Predictability: An Introduction. Victoria, BC: LeanPub.
Weibull (1951)
Waloddi Weibull. "A Statistical Distribution Function of Wide AppHcability."Journal ofApplied Mechanics, 18(3): 293-97.
Wikipedia (2015a)
"Fitness Landscape." Wikipedia. https://en.wikipedia.org/ wiki/Fitness_landscape (accessed October 30,2015).
Wikipedia (2015b)
"Monte Carlo Method." Wikipedia. https://en.wikipedia. org/wiki/Monte_Carlo_method (accessed December 11, 2015).
Wikipedia (2015c)
"Stationary Process." Wikipedia. https://en.wikipedia.org/ wiki/Stationarv_process (accessed May 27,2015).
Womack (2003)
James P. Womack and Daniel T.Jones. Lean Thinking: Banish Waste and Create Wealth in Your Corporation. London: Simon & Schuster.
Yoshima (2013)
Rodrigo Yoshima. "Management and Change—Avoiding the Rocks." Lean Kanban North America, United States: SHdeShare. http://www.sbdeshare.net/rodrigoy/ management-and-change-avoidin (accessed April 5,2016).
References	73