kanban boards, (cont.)
Kanbanraum, 18,19
options, 14
outstanding requests, 14
pool of ideas, 14
practical restraints, 18
socializing design, 28
swimlanes, 18, 55
varying designs, 18
Work in Progress Limits column, 13 Kanban Change Leadership
(Leopold), 45 Kanban From the Inside (Burrows),
45,59 Kanban in Action (Hammarberg),46 Kanban Litmus Test, 27
customer contract, 30
customer interface, 30
management behavior, 29-30
service business model, 31 Kanban Meeting, 25, 33, 51-52 The Kanban Method, vii, 51 kanbans,xi, 1, 48
kanban systems, xi, 1,15,18,27,30, 51,52,54
commitment point, 13-14
commitment to deliver work item, 48
delivery point, 13
depth-wise growth, 43
designing, 28
flowing to point of realizing value, 4
height-wise growth, 42-43
improved value, 31
items discarded prior to
commitment point, 50
managing flow, 21-22
options, 49
Personal Kanban, 42
portfolio, 43
product or service delivery, 43
replenishment, 49
risk management, 31
service levels, 22
signals indicating demand or available capacity, 51
signals to limit work in progress (WiP), 13
teams, 43
visual display, 51
width-wise growth, 41 Kanban values
agreement, 4
balance, 4
collaboration, 4
customer focus, 4
flow, 4
leadership, 4
respect, 3, 5
transparency, 3
understanding, 4 Kniberg, Henrik, 64 knowledge work, 1,51,52, 55 knowledge worker, 63