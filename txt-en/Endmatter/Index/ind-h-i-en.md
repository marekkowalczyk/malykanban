H,l
height-wise growth, 42-43 The Impact of Agile, Quantified
(Maccherone), 60 implementation, negotiating, 28
implement feedback loops, 23-26 improve collaboratively, evolve
experimentally, 26 information, sharing openly, 3 intangible service, 48
Kanban, xi, 1, 51,52, 53, 59 application in existing Scrum
implementation, 55 application to workload of
individual or small team, 53 caricatures of, viii expanding application of, 41-44 foundational principles, 9-11 introducing to organizations,
27-31 learning more about, 45-46 roles, 33
shortest possible guide to, 61, 68 standard of quality when taught
or practiced, v Systems Thinking approach,
28-29 test for accessing progress with, 27 training, v
use with other processes, viii using in new context, 55 varying implementations, vii Kanban: Successful Evolutionary Change for Your Technology Business (Anderson), viii, 45 Kanban and Scrum: Making The Most of Both (Kniberg and Skarin), 64 kanban boards, xi, 1,13,18-19,47, 51 activity column, 13 blockers, 47 cards, 55 columns, 54 describing work items, 19

Index
81
