Ladas, Corey, 64
The Landmarks of Tomorrow
(Drucker), 63 leadership, 4
Lead Time (LT), 14,15,16,22,30, 48,49,50,52,53,54,55,56, 57,58,62 averaging, 16, 39 Control Charts, 39 Distribution Histograms, 39 scatterplots, 37,38 Lead Time, Approximate Average,
15-16 Lean Flow paradigm, 26, 61 Lean Lexicon (Shook), 63 Lean Manufacturing, 46 limit work in progress, 19-20,53 Little's Law, 15-16, 52

82
Index
alternate formulation, 15 graphically demonstrating, 15 Time in Process (TiP), 64
