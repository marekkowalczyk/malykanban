
safe-to-fail, 8 scatterplots, 38,48,49,55 Scrum, 64
application of Kanban in existing, 55
use with Kanban, viii Scrumban, 55 Scmmban (Ladas), 64 selecting for fitness, 26 self-knowledge, 4 service business model, 31 Service Delivery Manager, 33 service delivery principles, 10-11

84
Index
Service Delivery Review, 25
Service Fitness Threshold, 22
service improvement, 23
Service Level Agreement, 22, 30, 36
Service Level Capability, 22
Service Level Expectation, 22, 30
Service Manager, 33
Service Orientation Agenda, 7, 8
Service Request Manager, 33
services, 1, 55,58
agreement with customer, 14
classes of, 21,27,31,48,49,55
customers, 1,4
daily coordination, 25
defining, managing and improving, 51
delivery of, 1, 8,43
delivery of work items, 48
direction to, 25
effectiveness, 25
feedback, 48
fit for purpose, 8,25,28
forecasting when they will be delivered to customers, 35-40
identifying, 28
improved in isolation, 29
improving, 8
lead time, 20
losing sight of, 20
managing large networks, 51
not overburdened with work, 8
planning review, 25
risks to effective delivery, 25
selection of, 25
self-organization, 25
STATIK, 28-29
system leveling, 31
system liquidity, 31
true kanban systems, 30
understanding balance between and across, 25 sharing information openly, 3 Skarin, Mattias, 64
staff engagement, 8
stakeholders and respect, 8
standard services, 48
stand-up meeting, 25
start from what you do now method,
1, 9-10 state, 15,37,47,55,56,58 STATIK (Systems Thinking
Approach to Implementing Kanban), 27,28-29,55 Stationary (Wikipedia), 63 strategy alignment, 23 Strategy Review, 25 suboptimization, 29 survivability, 46,47 Survivability Agenda, 7, 8 sustainability, 46,47 Sustainability Agenda, 7-8 swimlanes, 18, 51, 55 System Lead Time, 14,15,49,52,56 system leveling, 31,56 system liquidity, 31, 56 systems, 55,58
entry and departure of work
items, 51 items under consideration, 14 responding to new and varied
requests for work, 56 scheduling and delivering work,
54 socializing design, 28 sources of dissatisfaction with
current, 28 synchronizing sub-processes
within, 56 understanding how it behaves as a whole, 28-29 systems thinkers, 63 Systems Thinking, 28-29, 46, 61