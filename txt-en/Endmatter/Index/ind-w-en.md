w
Weibull distributions, 62 Weighted Shortest Job First
(WSJF),49,54,57,61,64 Why Limit WIP: We Are Drowning in
Work (Benson), 46 width-wise growth, 41
WiP Limit, 1,23,51,54 management, 30 policies, 1 setting, 22 WiP, Approximate Average, 15-16 work
advantageous in innovation and
change, 50 collaboration, 4 different policies applied to
different types, 21 flow of value, 4,26 having too much partially
complete, 19 limiting, 19-20 managing flow, 21-22 moving resources and people
among, 56 observing, 19-20 optimizing, 19-20 pulled into system, 1 separating request from
commitment to do, 50 stages, vii system for scheduling and
delivering, 54 visualizing and process it goes through, 18-19 workflow, 47,54,58 activities, 47 modeling, 28 work in progress, 54
limiting, 18, 52 Work in Progress Limit (WiP
Limit), 13,18,58 Work in Progress (WiP), 1,13,14, 16,47,52,54,58,60 average, 39
limiting, 16,19-20,51 signals to limit, 13 work items, 21,47,53,58 aborted, 19 amount of time in progress, 47,54
Index
archetypes characterizing how
value of items changing, 21 average time between completion
of, 56 benefit completed without delay
and benefit if delayed, 49 blocked, 47 categories, 48 commitment to deliver, 48 considered to be delivered or
complete, 50 cost of delay, 21,47,48,49,57,
65-66 describing, 19 discarding after commitment
point, 47 entering system or state, 58 entry and departure of, 51 flowing through stages of process,
13-16 held ahead of later activity, 54 moving to commitment point, 25 not started until work is
completed, 19 number exiting system or subsystem per unit of time, 50,
56
people collaborating to produce,
55 period in process under consideration, 15 policies, 23, 54 precedence, 57
rate at which delivered, 14-15 rate at which value is decaying,
57 ratio of time spent working on,
51 records of issues that have
blocked, 48 removing from process under
consideration, 50 states, 47,56,58 sum of times actively worked on,
57 time between starting and
completing, 49-50, 52 time customer waits for, 49 visual representation of, 48