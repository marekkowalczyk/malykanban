abort/aborted, 19,47,48,50,52
action
constraints on, 22 right to carry out, 53
Actionable Agile Metrics for Predictability: An Introduction (Vacanti), 61
activities, 47,54, 58
activity column, 13
adaptive, 7, 8
agendas, 7,47, 57
Service Orientation Agenda, 7, i Survivability Agenda, 7, 8 Sustainability Agenda, 7-8
AgeofWiP,39,47
Agile organizations, viii
Agile projects, 62
agreement, 4
Alternative Path to Agility, v
Anderson, David, viii, xi, 61
Antifragile: Things That Gain from Disorder (Taleb), 62
antifragility and hierarchies, 62
