pace, sustainable, v, 7-8 people, maximizing use of, 20 performance, 7, 8 Personal Kanban, 42, 53 Personal Kanban (Benson), 46, 62 policies, 1,13,48,51,53,58
capacity allocation and balancing, 23
Definition of Done/Definition of Ready, 23, 53
governing selection of work items, 54
making explicit, 22-23
not intuitively obvious, 22
processes, 22
queuing discipline, 31
visualizing, 18-19
WiP limits, 23
work items, 23 pool of ideas, 14 portfolio, 43 Practices of Kanban, General, 17
implement feedback loops, 23-26
improve collaboratively, evolve experimentally, 26
limit work in progress, 19-20
make policies explicit, 22-23
manage flow, 21-22
visualize, 18-19 predictability of supply, 22

Index
83
probabilistic forecasting, 35-40,50, 53,61 distribution data, 40 historical data, 36 range estimate, 36 relying on single value, 40 processes
arrivals and departures, 39, 49
constraint, 53
diversity, 8
end under consideration not
delivery point, 15 expressed as workflow and
policies, 22 future changes, 8 Kanban applied prior to
commitment point, 14 Kanban Method applied to, 53-54 knowledge-discovery steps, 13 out of control, 48 policies, 13,22 queue or stage without WiP limit, 57 Processing Rate, 56 Product Manager, 33 Product Owner, 33 products
delivery of, 43
ordering and selecting potential new features, 64-66 protokanban, 27,52,53-54, 61 pull system, 1,19,50,52,54,58 push system, changing to pull system, 19
