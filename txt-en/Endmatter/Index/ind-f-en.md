F
Factory Physics (Hopp), 63 feedback
cadences, 24-26
opportunities, 24-26 feedback loops
cadence network diagram, 26
customer deliveries, 24
flow, 24
operational coordination, 23
replenishment, 24
risk management, 23
service improvement, 23
strategy alignment, 23 first in first out (FIFO), 54 fitness landscape, 26, 51 Fitness Landscape (Wikipedia), 63 fixed date services, 48

80
Index
flow, 4,24, 60
blockers, 21
bottlenecks, 21
managing, 21-22
relationship with customers, 22 Flow Efficiency, 51,54, 57 Flow Manager, 33 Flow Master, 33 flow systems, 1,13-16, 51
Control Charts, 39
Cumulative Flow Diagrams (CFDs), 39
defined commitment, 52
delivery points, 52
Distribution Histograms, 39
flow metrics, 36
forecasting outcomes, 53
graphs, 37-39
Kanban Method applied to, 53-54
Fittle's Faw, 15-16
relationship between attributes of queues and, 52
Run Charts, 39
scatterplots, 38
work in progress limits, 52 focus, improving, 7-8 Forecast Date distribution, 36 forecasting, 35-40 foundational principles
change management principles, 9-10
service delivery principles, 10-11
