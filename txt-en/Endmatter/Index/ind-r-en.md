Raleigh distribution of Waterfall projects, 62
real options, 53
Real-World Kanban (Skarin), 46
Reddy, Ajay, 64
Reinertsen, Don, 64-66
replenishment, 24, 49,54, 58
Replenishment Meeting, 25,30, 33,
54 replenishment policies, 23 Resource Efficiency, 51,54 resources
amount of time actively working on work item, 54
deploying to maximize delivery value, 25
maximizing use of, 20
right to use, 53 respect, 3, 5
stakeholders, 8 reviews
driving evolutionary change, 24-26
information flow, 26
requests for change, 26
time periods between, 24-26 risk management, 23 Risk Review, 25 roles, 33 Run Charts, 39,48,49,54,55
