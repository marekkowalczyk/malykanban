cadences, 24-26,48, 52 agenda, 25
Delivery Planning Meeting, 25
Kanban Meeting, 25
Operations Review, 25
Replenishment Meeting, 25
Risk Review, 25
Service Delivery Review, 25
Strategy Review, 25 capabilities
analyzing, 28
balancing, 4, 8 capacity allocation and balancing, 23 cards, 48,55,58
cash flows, present value of future, 53 change
keeping and amplifying, 26
minimizing resistance to, 10
starting point for, 8 change management principles,
9-10 classes of service, 21,27,28,31, 48,
49,55 GMMI Survival Guide (Garcia and
Turner), 61 collaboration, 4, 8 column, 13 commitment, 52, 56 commitment point, 13,18, 47, 48, 50,52-54
Kanban applied to processes prior to, 14
moving items over, 25
time item is in process, 14 competitive, 7, 8 complex systems behavior, 22 Control Charts, 39,48,49,54
79
controlled processes feedback loops,
23-26 Corbis, xi cost, 8,36-37 Cost of Delay (CoD), 21,48,49,57,
65-66 Cumulative Flow Diagram (CFD),
15,37,39,49,55 customer contract, 30 customer deliveries, 24, 47 customer focus, 4 customer interface, 30 Customer Lead Time, 14,22, 49, 52 customers, 1,20
agreement with service, 14 demand expressed as average unit
production time, 56 external to services, 4 meeting and exceeding needs and
expectations, 8 monitoring and planning delivery
to, 25 providing services fit for purpose,
8 relationship with, 22 time waiting for item, 14 customer satisfaction, 7, 8 Cycle Time (CTl, CT2), 49-50,56, 59,64
