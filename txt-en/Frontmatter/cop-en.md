Essential Kanban Condensed
Copyright © 2016 David J. Anderson and Andy Carmichael PhD, FBCS
ISBN 978-0-9845214-2-5
All rights reserved. This publication is protected by copyright, and permission must be obtained from the publisher prior to any reproduction, storage in a retrieval system, or transmission in any form or by any means, electronic, mechanical, photocopying, recording, or likewise.
Contact info@leankanban.com for rights requests, customized editions, and bulk orders.
Essential Kanban CondensedCan be downloaded via leankanban.com/guide. Additional print copies of this and other Kanban publications can be purchased via shop.leankanban.com.
Library of Congress Cataloging in Publication Data
Names: Anderson, David J., 1967- | Carmichael, Andy.
Title: Essential Kanban condensed / David J Anderson [and] Andy Carmichael. Description: First edition. | [Seattle, Washington] : Lean Kanban University Press, 2016. | Series: [Essential Kanban] ; [2] | Includes bibliographical references and index.
Identifiers: ISBN 978-0-9845214-2-5
Subjects: LCSH: Just-in-time systems. | Lean manufacturing. | Production management. | Computer software—Development—Management. Classification: LCCTS157.4 .A544 2016 I DDC 658.51—dc23
6 6
Visit edu.leankanban.com for a list of accredited „,(1U.^?I15l?1!1 Kanban classes and information about becoming a Kanban trainer.
Visit services.leankanban.com or email sales@ leankanban.com for information regarding private
training or consulting for your organization.